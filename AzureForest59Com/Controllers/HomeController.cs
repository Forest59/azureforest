﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
           // ViewBag.Message = "Your application description page.";

           // NameValueCollection coll;

            System.Collections.Specialized.NameValueCollection coll;
            int loop1, loop2;

            coll = Request.Headers;
            String msg = "";
            String[] arr1 = coll.AllKeys;
            for (loop1 = 0; loop1 < arr1.Length; loop1++)
            {
                msg += String.Format("Key: " + arr1[loop1] + "</br>");
                string[] arr2 = coll.GetValues(arr1[loop1]);
                for (loop2 = 0; loop2 < arr2.Length; loop2++)
                {
                    msg += String.Format("&nbsp;&nbsp;&nbsp;Value: " + loop2 + ": " + Server.HtmlEncode(arr2[loop2]) + "</br>");
                }
            }
            ViewBag.Message = msg;
            return View();
        }

        public ActionResult Contact()
        {
           // ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Mymobiles()
        {
            return View();
        }

        public ActionResult Myforest()
        {
            return View();
        }

        public ActionResult Mydevelop()
        {
            return View();
        }
    }
}